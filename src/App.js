import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import "./scss/app.scss";
import Header from "./components/Header";
import AboutUs from "./components/AboutUs";
import Prices from "./components/Prices";
import HoneysDescriptions from "./components/HoneysDescriptions";
import FormToOrder from "./components/FormToOrder";
import FormToMessage from "./components/FormToMessage";
import Footer from "./components/Footer";
import Admin from "./components/admin/Admin.jsx";

function App() {

  const [aboutUsFromDb, setAboutUsFromDb] = useState({});
  const [pricesFromDb, setPricesFromDb] = useState([]);

    useEffect(() => {
        fetch('/app', {
        })
        .then(response => response.json())
        .then(data => {
          setAboutUsFromDb(data.aboutUs);
          setPricesFromDb(data.prices);
        })
    }, []);

  return (
    <Router>
    <Switch>
    <Route exact path="/">
      <Header/>
      <AboutUs aboutUs={aboutUsFromDb}/>
      <Prices prices={pricesFromDb}/>
      <HoneysDescriptions honeysDescriptions={pricesFromDb}/>
      <FormToOrder products={pricesFromDb}/>
      <FormToMessage/>
      <Footer/>
      </Route>
      <Route path="/admin1">
        <Admin />
      </Route>
      </Switch>
    </Router>
  );
}

export default App;
