import React, {useState} from 'react';

export default function FormToMessage() {

    const [message, setMessage] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [response, setResponse] = useState("");
    const [style, setStyle] = useState("bad-answer");

    function onSubmit(e) {
        e.preventDefault();
        if(message.length<2 || name.length<2 || email.length<4) {
            setResponse("Podaj swoje imię, treść wiadomości oraz poprawny adres e-mail!")
        } else {
            setStyle("good-answer")
            setResponse("Wiadomość wysłana!")
            const data = {
                message,
                name,
                email
            };
            fetch('/message', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify(data),
                
            });
        }
    }

    return (
        <section className="form-to-message" id="contact">
            <article className="form-to-message__main">
                <p className="form-to-message__text form-to-message__text__description">Chcesz dowiedzieć się więcej, napisz do nas (a jeśli jesteś już zdecydowany, złóż zamówienie <span className="form-to-message__main__text form-to-message__main__text--bold"><a className="form-to-message__main__text form-to-message__main__text__link" href="#to-order">tutaj</a></span>).</p>
                <form onSubmit={e=>onSubmit(e)} className="form-to-message__main__form">
                    <label className="form-to-message__main__form__input">
                        <p className="form-to-message__main__form__input__text">Twoja wiadomość</p>
                        <textarea onChange={e=>setMessage(e.target.value)} value={message} className="form-to-message__main__form__input__content"/>
                    </label>
                    <label className="form-to-message__main__form__input">
                        <p className="form-to-message__main__form__input__text">Twój podpis</p>
                        <input type="text" onChange={e=>setName(e.target.value)} value={name} className="form-to-message__main__form__input__content"/>
                    </label>
                    <label className="form-to-message__main__form__input">
                        <p className="form-to-message__main__form__input__text">Twój e-mail</p>
                        <input type="email" onChange={e=>setEmail(e.target.value)} value={email} className="form-to-message__main__form__input__content"/>
                    </label>
                    <button type="submit" className="form-to-message__main__form__input__button">Wyślij</button>
                </form>
                <p className={style}>{response}</p>
            </article>
             <article className="form-to-message__map"><iframe className="form-to-message__map__content" title="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2497.7843156739973!2d18.217401!3d51.241467!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4710778ff07eb899%3A0x44818eaad5f7c1f4!2sMieleszyn%2082%2C%2098-430%20Mieleszyn!5e0!3m2!1spl!2spl!4v1615296976309!5m2!1spl!2spl"></iframe></article>
        </section>
    )
}
