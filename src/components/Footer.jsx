import React from 'react'

export default function Footer() {
    return (
        <footer className="footer">
            <p className="footer__left">
                <a href="mailto:k.wojtas172@gmail.com" className="footer__left__link">
                    Created by Kamil Wojtas
                </a>
                </p>
            <p className="footer__right">Copyright ©2021</p>
        </footer>
    )
}
