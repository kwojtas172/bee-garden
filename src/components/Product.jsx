import React from 'react'

export default function Product({nameOfProduct, priceOfProduct, keyOfProduct}) {
    return (
        <li key={keyOfProduct} className="prices__list__element">
            <p className="prices__list__element__text">{nameOfProduct}</p>
            <p className="prices__list__element__text prices__list__element__text--space">-</p>
            <p className="prices__list__element__text">{priceOfProduct} PLN</p>
        </li>
    )
}
