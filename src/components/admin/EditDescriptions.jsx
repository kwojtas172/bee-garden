import React, {useState, useEffect} from 'react';
import { useCookies } from 'react-cookie';

export default function EditDescriptions({descriptions}) {

    const [honeyDescriptions, setHoneyDescritpions] = useState([]);
    const [btnAvailableValue, setBtnAvailableValue] = useState(0);
    const [cookies] = useCookies("login");
    const [showMoreLess, setShowMoreLess] = useState(true);

    useEffect(() => {
        setHoneyDescritpions(Array.from(descriptions))
    }, [descriptions]);

    const patchEditDescription = e => {
        e.preventDefault();
        setBtnAvailableValue(0);
        const data = {
            toRead: honeyDescriptions,
            login: cookies.login
        }
        fetch('/admin/honey-descriptions', {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
        })
        .then(res=>res.json())
        .then(data=>console.log(data))
    }

    const handleBtnEditCancel = e => {
        e.preventDefault();
        e.target.innerText = e.target.innerText === "Edytuj" ? "Anuluj" : "Edytuj";
        setBtnAvailableValue(e.target.innerText === "Edytuj" ? 0 : Number(e.target.dataset.id))
    }


    const handleHoney = e => {
        e.preventDefault();
        let copyArr = [...descriptions];
        copyArr.filter(honey=>honey.key === Number(e.target.dataset.id))
        .forEach(honey => {
            switch (e.target.dataset.name) {
                case "name" :
                    honey.name = e.target.value;
                    break;
                case "description" :
                    honey.description = e.target.value;
                    break;
                case "price" :
                    honey.price = Number(e.target.value);
                    break;
                case "quantity" :
                    honey.quantity = Number(e.target.value);
                    break;
                default :
                    return;
            }
        });
        setHoneyDescritpions(copyArr)
    }

    return (
        <div className="admin-section">
            <h2>Miód - opis i ceny</h2>
            <button className="admin-section__btn" onClick={() => setShowMoreLess(!showMoreLess)}>{showMoreLess ? "Pokaż więcej" : "Pokaż mniej"}</button>
            {!showMoreLess && <ul className="admin-section__list">
                {honeyDescriptions.map(honey=><li className="admin-section__list__element">
                    <form className="admin-section__form">
                        <input className="admin-section__form__input" value={honey.name} onChange={e=>handleHoney(e)} data-id={honey.key} data-name="name" disabled={btnAvailableValue === honey.key ? false : true} />
                        <textarea className="admin-section__form__text" value={honey.description} onChange={e=>handleHoney(e)} data-id={honey.key} data-name="description" disabled={btnAvailableValue === honey.key ? false : true} />
                        <input className="admin-section__form__input" type="number" value={honey.price} onChange={e=>handleHoney(e)} data-id={honey.key} data-name="price" disabled={btnAvailableValue === honey.key ? false : true} />
                        <input className="admin-section__form__input" type="number" value={honey.quantity} onChange={e=>handleHoney(e)} data-id={honey.key} data-name="quantity" disabled={btnAvailableValue === honey.key ? false : true} />
                        <button className="admin-section__form__button" data-id={honey.key} onClick={e=>handleBtnEditCancel(e)} disabled={(btnAvailableValue !== 0 && btnAvailableValue !== honey.key) ? true : false}>{btnAvailableValue === honey.key ? "Anuluj" : "Edytuj"}</button>
                        <button className="admin-section__form__button" onClick={e=>patchEditDescription(e)} disabled={btnAvailableValue === honey.key ? false : true}>Zapisz</button>
                    </form>
                    </li>)}
            </ul>}
        </div>
    )
}
