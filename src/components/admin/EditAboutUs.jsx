import React, {useState, useEffect} from 'react';
import { useCookies } from 'react-cookie';

export default function EditAboutUs({aboutUs}) {

    const [description, setDescription] = useState("");
    const [btnEditCancel, setBtnEditCanel] = useState("Edytuj");
    const [successMsg, setSuccessMsg] = useState(false);
    const [cookies] = useCookies("login");
    const [showMoreLess, setShowMoreLess] = useState(true);

    useEffect(() => {
        setDescription(aboutUs.description);
    }, [aboutUs.description])


    const handleBtnEditCancel = e => {
        e.preventDefault();
        setBtnEditCanel(btnEditCancel === "Edytuj" ? "Anuluj" : "Edytuj");
        if(btnEditCancel === "Anuluj") setDescription(aboutUs.description);
        if(successMsg === true) setSuccessMsg(false);
    }

    const saveDescription = e => {
        e.preventDefault();
        setSuccessMsg(true);
        setBtnEditCanel("Edytuj");
        const data = {
            toRead: {
                title: aboutUs.title,
                description
            },
            login: cookies.login
        }
        fetch('/admin/about-us', {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
        })
        .then(res=>res.json())
        .then(data=>console.log(data))
    }
    

    return (
        <div className="admin-section">
            <h2 className="admin-section__title">{aboutUs.title}</h2>
            <button className="admin-section__btn" onClick={() => setShowMoreLess(!showMoreLess)}>{showMoreLess ? "Pokaż więcej" : "Pokaż mniej"}</button>
            {!showMoreLess && <form className="admin-section__form">
                <textarea className="admin-section__form__text" value={description} onChange={e=>setDescription(e.target.value)} disabled={btnEditCancel === "Edytuj" ? true : false} />
                <button className="admin-section__form__button" onClick={e=>handleBtnEditCancel(e)}>{btnEditCancel}</button>
                <button className="admin-section__form__button" onClick={e=>saveDescription(e)} disabled={btnEditCancel === "Edytuj" ? true : false}>Zapisz</button>
            </form>}
            {successMsg && <span className="admin-section__communicate">Edycja zakończona sukcesem</span>}
        </div>
    )
}
