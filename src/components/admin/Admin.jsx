import React, {useState, useEffect} from 'react';
import { useCookies } from 'react-cookie';
import EditDescriptions from "./EditDescriptions";
import EditAboutUs from "./EditAboutUs";
import EditOrders from "./EditOrders";
import EditMessages from "./EditMessages";

export default function Admin() {

    const [isLoggin, setIsLoggin] = useState(false);
    const [password, setPassword] = useState("");
    const [wrongInfo, setWrongInfo] = useState(false);
    const [descriptions, setDescriptions] = useState({});
    const [aboutUs, setAboutUs] = useState({});
    const [orders, setOrders] = useState({});
    const [messages, setMessages] = useState({});
    const [cookies, setCookie] = useCookies("login");

    useEffect(() => {
        isCheckPassword(cookies)
    }, [cookies]);

    const isCheckPassword = (cookies) => {
        fetch('/admin', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(cookies),
            
        })
        .then(res=>res.json())
        .then(data=>{
            if(data.response === "Password is wrong!") {
                setIsLoggin(false)
            }
            if(data.response === "Password is good!") {
                setIsLoggin(true);
                loadData();
            }

        })
        .catch(err => console.log(err))
    }

    const loadData = async() => {
        fetch('/admin', {
        })
        .then(response => response.json())
        .then(data => {
           setDescriptions(data.honey.prices);
           setAboutUs(data.honey.aboutUs);
           setOrders(data.ordersAndMessages.orders);
           setMessages(data.ordersAndMessages.messages);
        })
    }

    const sendPassword = (e) => {
        e.preventDefault();
        setCookie("login", password, {path: "/admin1", maxAge: "31557600"});
        const cookies = {login: password}
        isCheckPassword(cookies);
        if(isLoggin) {
            window.location.reload();
        } else {
            setWrongInfo(true);
        }
    }

    if(isLoggin) {
        return (
                <div className>
                    <EditAboutUs aboutUs={aboutUs}/>
                    <EditDescriptions descriptions={descriptions}/>
                    <EditOrders orders={orders}/>
                    <EditMessages messages={messages}/>
                </div>
        )
    }
    else {
        return (
            <div>
                <form onSubmit={e => sendPassword(e)}>
                    <input type="password" onChange={e=>setPassword(e.target.value)} />
                    <button>Wyślij</button>
                </form>
                {wrongInfo && <p>Niepoprawne hasło, spróbuj ponownie!</p>}
            </div>
        )
    }
}
