import React from 'react';

export default function EditOrders({orders}) {

    const [ordersArr, setOrdersArr] = React.useState([]);
    const [showMoreLess, setShowMoreLess] = React.useState(true);

    React.useEffect(() => {
        let arr = Array.from(orders);
        setOrdersArr(arr)
    },[orders])


    const sendExampleMail = (e, id) => {
        e.preventDefault();

        let copyOrders = [...ordersArr];
        let orderTochange = copyOrders.filter(order=>order.id===id)[0];
        if (orderTochange.isConfirmed === true) {
            deleteAfterConfirm(orderTochange.id)
        } else {
            const data = orderTochange;
            fetch('/admin/order', {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
        })
        .then(res=>res.json())
        .then(data=>console.log(data))
            orderTochange.isConfirmed = true;
            setOrdersArr(copyOrders)
            }
    }

    const deleteAfterConfirm = id => {
        const data = {id};
        fetch('/admin/order', {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
        })
        .then(res=>res.json())
        .then(data=>console.log(data))

        let copyOrders = [...ordersArr];
        copyOrders = copyOrders.filter(order=>order.id!==id)
        setOrdersArr(copyOrders)
    }
    
    return (
        <div>
            <h2 className="admin-section__primary-title">Zamówienia</h2>
            <button className="admin-section__btn" onClick={() => setShowMoreLess(!showMoreLess)}>{showMoreLess ? "Pokaż więcej" : "Pokaż mniej"}</button>
            {!showMoreLess && <div>
                <h3 className="admin-section__secondary-title">Do potwierdzenia</h3>
            <ul className="admin-section__orders-list">
            {ordersArr
            .filter(order=>!order.isConfirmed)
            .map(order=>{
                return (
                    <li key={order.id}>
                        <p className="admin-section__paragraphe">
                            <span>Wybrany miód: {order.kindOfHoney}</span><br></br>
                            <span>Ilość słoików: {order.amountOfHoney}</span><br></br>
                            <span>Imię i nazwisko: {order.name}</span><br></br>
                            <span>Adres: {order.adress}</span><br></br>
                            <span>Numer telefonu: {order.phone}</span><br></br>
                            <span>E-mail: {order.email}</span><br></br>
                        </p>
                        <button className="admin-section__form__button" onClick={e=>sendExampleMail(e, order.id)}>{order.isConfirmed ? "Przesuń z powrotem" : "Potwierdź rezerwację (wyślij e-mail)"}</button><br></br><br></br></li>
                )
            })}
            </ul>
            <h3 className="admin-section__secondary-title">Potwierdzone</h3>
            <ul className="admin-section__orders-list">
            {ordersArr
            .filter(order=>order.isConfirmed)
            .map(order=>{
                return (
                    <li key={order.id}>
                        <p className="admin-section__paragraphe">
                            <span>Wybrany miód: {order.kindOfHoney}</span><br></br>
                            <span>Ilość słoików: {order.amountOfHoney}</span><br></br>
                            <span>Imię i nazwisko: {order.name}</span><br></br>
                            <span>Adres: {order.adress}</span><br></br>
                            <span>Numer telefonu: {order.phone}</span><br></br>
                            <span>E-mail: {order.email}</span><br></br>
                        </p>
                        <button className="admin-section__form__button" onClick={e=>sendExampleMail(e, order.id)}>{order.isConfirmed ? "Usuń z listy" : "Potwierdź rezerwację (wyślij e-mail)"}</button><br></br><br></br></li>
                )
            })}
            </ul>
                </div>}
        </div>
    )
}
