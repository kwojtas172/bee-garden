import React from 'react';

export default function EditMessages({messages}) {

    const [messagesArr, setMessagesArr] = React.useState([]);
    const [showMoreLess, setShowMoreLess] = React.useState(true);

    React.useEffect(() => {
        setMessagesArr(Array.from(messages))
    }, [messages]);



    const deleteMsg = id => {
        const data = {id};
        fetch('/admin/message', {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
        })
        .then(res=>res.json())
        .then(data=>console.log(data))

        let copyOrders = [...messagesArr];
        copyOrders = copyOrders.filter(order=>order.id!==id)
        setMessagesArr(copyOrders)
    }

    return (
        <div className="admin-section__messages">
            <h2 className="admin-section__primary-title">Wiadomości</h2>
            <button className="admin-section__btn" onClick={() => setShowMoreLess(!showMoreLess)}>{showMoreLess ? "Pokaż więcej" : "Pokaż mniej"}</button>
            {!showMoreLess && <ul>
            {messagesArr.map(msg=><li><p className="admin-section__paragraphe"><span>{msg.email}</span><span>{msg.message}</span><span>{msg.name}</span></p><button className="admin-section__form__button" onClick={()=>deleteMsg(msg.id)}>usuń wiadomość</button></li>)}
            </ul>}
        </div>
    )
}
