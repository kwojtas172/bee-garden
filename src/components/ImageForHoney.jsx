import React, { useState, useEffect } from 'react';
import image1 from "../images/linden.jpg";
import image2 from "../images/buckwheat.jpg";
import image3 from "../images/acacia.jpg";
import image4 from "../images/honey-dew.jpg";
import image5 from "../images/heather.jpg";
import image6 from "../images/colza.jpg";
import image7 from "../images/honey-823614_640.jpg";

export default function ImageForHoney({img}) {

    const [currentImg, setCurrentImg] = useState(image1);

    function handleCurrentImg(num) {
            num = Number(num);
            switch (num) {
                case 1:
                    setCurrentImg(image1);
                    break;
                case 2:
                    setCurrentImg(image2);
                    break;
                case 3:
                    setCurrentImg(image3);
                    break;
                case 4:
                    setCurrentImg(image4);
                    break;
                case 5:
                    setCurrentImg(image5);
                    break;
                case 6:
                    setCurrentImg(image6);
                    break;
                case 7:
                    setCurrentImg(image7);
                    break;
                default:
                    setCurrentImg(currentImg);
            }
        }

        useEffect(() => {
            handleCurrentImg(img);
        });

    return (
        <img className="honeys-descriptions__section-of-descriptions__one-description__image" src={currentImg} alt="" />
    )
}
