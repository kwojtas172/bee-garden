import React from 'react';
import image from "../images/bee-705412_640.png";

export default function AboutUs({aboutUs}) {
    
    return (
        <section className="about-us" id="about-us">
            <h2 className="about-us__title">{aboutUs.title}</h2>
            <p className="about-us__descritpion">{aboutUs.description}</p>
            <img className="about-us__img" src={image} alt="bee"/>
        </section>
    )
}
