import React, {useState, useEffect} from 'react';
import logo from "../images/header-logo.jpg";


export default function Header() {

    const [classMenuToHidden, setClassMenuToHidden] = useState("");
    const [classHamburger, setClassHamburger] = useState("header__menu__hamburger");

    const handleStatusMenu = () => {
        setClassMenuToHidden(classMenuToHidden ==="header__menu__list--hidden" ? "" : "header__menu__list--hidden");
        setClassHamburger(classHamburger === "header__menu__hamburger" ? "header__menu__hamburger header__menu__hamburger--cross" : "header__menu__hamburger")
    }

    useEffect(() => {
        if(window.innerWidth <=720) setClassMenuToHidden("header__menu__list--hidden");

        window.addEventListener("resize", () => {
            const width = window.innerWidth;
            if(width <= 720) {
                setClassMenuToHidden("header__menu__list--hidden")
            } else {
                setClassMenuToHidden("")
            };
        })
    }, [])

    return (
        <header className="header">
                <div className="header__wrapper-title">
                    <h1 className="header__wrapper-title__main-title">Pasieka "Pod Lasem"</h1>
                    <p className="header__wrapper-title__description">Miód własnej produkcji</p>
                </div>
                <nav className="header__menu">
                    <span className={classHamburger} onClick={()=>handleStatusMenu()}></span>
                    <ul className={`header__menu__list ${classMenuToHidden}`}>
                        <li className="header__menu__list__element"><a href="#about-us" className="header__menu__list__element__link">O nas</a></li>
                        <li className="header__menu__list__element"><a href="#honey-kinds" className="header__menu__list__element__link">Rodzaje miodu</a></li>
                        <li className="header__menu__list__element"><a href="#to-order" className="header__menu__list__element__link">Złóż zamówienie</a></li>
                        <li className="header__menu__list__element"><a href="#contact" className="header__menu__list__element__link">Kontakt</a></li>
                    </ul>
                </nav>
                <img className="header__image" alt="apiary" src={logo}></img>
            </header>
    )
}
