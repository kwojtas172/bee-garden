import React from 'react';
import Product from "./Product";
import image from "../images/bee-2022492_640.png";

export default function Prices({prices}) {

    return (
        <section className="prices">
            <h2 className="prices__title">Cennik</h2>
            <h3>(za 1l miodu)</h3>
            <ul className="prices__list">
                {prices.map(product => {
                    return <Product nameOfProduct={product.name} priceOfProduct={product.price} key={product.key} />
                })}
            </ul>
            <img src={image} alt="" className="prices__img"></img>
        </section>
    )
}
