import React, { useState } from 'react';
import ImageForHoney from "./ImageForHoney";


export default function HoneysDescriptions({honeysDescriptions}) {

    const [btnValue, setBtnValue] = useState(1);
    const [classForFirstDescription, setClassForFirstDescription] = useState(" honeys-descriptions__section-of-choices__button--first-btn");

    function handleDescription(value) {
        setBtnValue(value);
    }

    return (
        <section className="honeys-descriptions" id="honey-kinds">
            <h2 className="honeys-descriptions__title">Rodzaje miodu</h2>
            <div className="honeys-descriptions__section-of-choices">
                {honeysDescriptions
                .filter((honey, idx) => idx === 0)
                .map(honey => {
                    return <button className={"honeys-descriptions__section-of-choices__button"+classForFirstDescription} onClick={e=>handleDescription(e.target.dataset.id)} data-id={honey.key} key={honey.key}>{honey.name}</button>
                })}
                {honeysDescriptions
                .filter((honey, idx) => idx !== 0)
                .map(honey => {
                    return <button className="honeys-descriptions__section-of-choices__button" onClick={e=>{handleDescription(e.target.dataset.id); setClassForFirstDescription("")}} data-id={honey.key} key={honey.key}>{honey.name}</button>
                })}
            </div>
            <div className="honeys-descriptions__section-of-descriptions">
                {honeysDescriptions
                .filter(honey => honey.key === Number(btnValue))
                .map(honey => {
                        return (
                            <div className="honeys-descriptions__section-of-descriptions__one-description" key={honey.key}>
                                <span className="honeys-descriptions__section-of-descriptions__one-description__name">{honey.name}</span>
                                <div className="honeys-descriptions__section-of-descriptions__one-description__description">{honey.description}</div>
                                <ImageForHoney img={btnValue} />
                            </div>
                        )
                })}
            </div>
        </section>
    )
}
