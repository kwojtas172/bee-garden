import React, {useState} from 'react';
import image1 from "../images/form-to-order-1.png";
import image2 from "../images/form-to-order-2.png";

export default function FormToOrder({products}) {

    const [adress, setAdress] = useState("");
    const [kindOfHoney, setKindOfMoney] = useState("Wybierz rodzaj miodu");
    const [amountOfHoney, setAmouuntOfHoney] = useState(0);
    const [name, setName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [email, setEmail] = useState("");

    const [isSubmit, setIsSubmit] = useState(false);

    function onSubmit(e) {
        e.preventDefault();
        console.log("function onSubmit is working");
        const data = {
            adress: adress,
            kindOfHoney: kindOfHoney,
            amountOfHoney: Number(amountOfHoney),
            name: name,
            phone: phoneNumber,
            email: email,
            isConfirmed: false
        }
        fetch('/order', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data),
            
        })

        setIsSubmit(true);
    }

    return (
        <section className="form-to-order" id="to-order">
            <h2 className="form-to-order__title">Złóż zamówienie</h2>
            {!isSubmit &&
            <form onSubmit={e=>onSubmit(e)} className="form-to-order__form">
                 <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Twoje imię i nazwisko</p>
                    <input type="text" required onChange={e=>setName(e.target.value)} value={name} className="form-to-order__form__input__content" />
                </label>
                <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Twój numer telefonu</p>
                    <input type="tel" pattern="[0-9]{9}" required onChange={e=>setPhoneNumber(e.target.value)} value={phoneNumber} className="form-to-order__form__input__content" />
                </label>
                <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Twój e-mail</p>
                    <input type="email" required onChange={e=>setEmail(e.target.value)} value={email} className="form-to-order__form__input__content" />
                </label>
                <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Twój adres</p>
                    <textarea type="address" required onChange={e=>setAdress(e.target.value)} value={adress} className="form-to-order__form__input__content"/>
                </label>
                <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Rodzaj miodu</p>
                    <select className="form-to-order__form__input__content" required onChange={e=>setKindOfMoney(e.target.value)} value={kindOfHoney}>
                        <option key="0" disabled="disabled">Wybierz rodzaj miodu</option> 
                        {products.map(honey=> {
                            return <option value={honey.name} key={honey.key}>{honey.name}</option>
                        })}
                    </select>
                </label>
                <label className="form-to-order__form__input">
                    <p className="form-to-order__form__input__text">Liczba słoików</p>
                    <input type="number" min="1" max="99" required onChange={e=>setAmouuntOfHoney(e.target.value)} value={amountOfHoney} className="form-to-order__form__input__content" />
                </label>
                <button type="submit" className="form-to-order__form__input__button">Wyślij</button>
            </form>}
            {isSubmit && <p>Zamówienie złożone, sprawdź skrzynkę e-mail!</p>}
            <img src={image1} alt="" className="form-to-order__img form-to-order__img--left"/>
            <img src={image2} alt="" className="form-to-order__img form-to-order__img--right"/>
        </section>
    )
}
