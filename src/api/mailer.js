const nodemailer = require("nodemailer");

function mailer(data, method) {

  let transporter = nodemailer.createTransport({
    host: "smtp.zoho.eu",
    port: 465,
    secure: true,
    auth: {
      user: "admin@pasieka-pod-lasem.pl",
      pass: "[PASSWORD]"
    },
  });

  let mailOptions;

  if (method === "automatic") {
    mailOptions = {
      from: '"Pasieka" <admin@pasieka-pod-lasem.pl>',
      to: data.email,
      subject: "Rezerwacja przyjęta",
      text: "Rezerwacja potw.",
      html: `<p>Przyjęliśmy Twoją rezerwację. Jest to wiadomość automatycznie wysłana przez system, dlatego poczekaj na kolejną wiadomość z potwierdzeniem rezerwacji. Dziękujemy!</p><p>Twoje zmówienie - rodzaj miodu: ${data.kindOfHoney}, liczba słoików: ${data.amountOfHoney}, Twój nr telefonu: ${data.phone}, id zamówienia: ${data.id} - jeśli nr telefonu nie zgadza się, skontaktuj się za pośrednictwem formularza dostępnego na stronie, podając w treści wiadomości ID zamówienia!</p>`,
    };
  }

  if(method === "manual") {
    mailOptions = {
      from: '"Pasieka" <admin@pasieka-pod-lasem.pl>',
      to: data.email,
      subject: "Rezerwacja potwierdzona",
      text: "Rezerwacja potw.",
      html: `<p>Potwierdziliśmy wstępnie Twoją rezerwację - poczekaj na rozmowę telefoniczą, podczas której potwierdzimy rodzaj zamówionego miodu, liczbę słoików oraz dane potrzebne do wysyłki. Dziękujemy!</p><p>Twoje zmówienie - rodzaj miodu: ${data.kindOfHoney}, liczba słoików: ${data.amountOfHoney}, Twój nr telefonu: ${data.phone}, id zamówienia: ${data.id} - jeśli nr telefonu nie zgadza się, skontaktuj się za pośrednictwem formularza dostępnego na stronie, podając w treści wiadomości ID zamówienia!</p>`,
    };
  }

transporter.sendMail(mailOptions, (err, info) => {
    if (err) throw err;
    console.log('Message sent: %s', info.messageId);
    transporter.close()
})

}

exports.function = mailer;
