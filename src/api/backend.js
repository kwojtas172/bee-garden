const express = require('express');
const app = express();
const port = process.env.PORT || 3001;
const fs = require('fs');
const cookieParser = require('cookie-parser');
const mailer = require("./mailer");
const path = require('path');

app.use(express.json());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, '../../build')))
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../../build', 'index.html'))
})

app.use('/admin1', (req, res) => {
  res.sendFile(path.join(__dirname, '../../build', 'index.html'))
})



const passToParse = fs.readFileSync("./admin.json", (err, data) => {
  if (err) throw err;
  return data
});
const password = JSON.parse(passToParse).password;

app.get('/app', (req, res) => {
  const db = fs.readFileSync("db.json", (err, data) => {
    if (err) throw err;
    return data;
});
  const dbToRead = JSON.parse(db);
  res.json(dbToRead)
  });

  let isLoadContentOfAdmin = false;
  

  app.post('/admin', function (req, res) {
    if(req.body.login === password) {
      isLoadContentOfAdmin = true;
      res.json({"response": "Password is good!"});
    } else res.json({"response": "Password is wrong!"})
  });

  app.get('/admin', function (req, res) {
    if(isLoadContentOfAdmin) {
      const db = fs.readFileSync("db.json", (err, data) => {
        if (err) throw err;
        return data;
    });
    const dbToRead = JSON.parse(db);

    const form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    const formToRead = JSON.parse(form);
    isLoadContentOfAdmin = false;
    res.json({"honey": dbToRead, "ordersAndMessages": formToRead})
    } else res.end()
  

  });

  app.post('/order', (req, res) => {
    let form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    form = JSON.parse(form);
    let orders = form.orders;
    let orderToPush = req.body;
    let id = Math.random().toString().substring(2);
    orderToPush.id = id;
    mailer.function(orderToPush, "automatic")
    orders.push(orderToPush);
    fs.writeFileSync("form.json", JSON.stringify(form));
    res.end();
  });

  app.post('/message', (req, res) => {
    let form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    form = JSON.parse(form);
    let messages = form.messages;
    let messageToPush = req.body;
    let id = messageToPush.name + messageToPush.email + Math.random();
    messageToPush.id = id;
    messages.push(messageToPush);
    fs.writeFileSync("form.json", JSON.stringify(form));
    res.end();
  });

  app.patch('/admin/about-us', (req, res) => {
    if(req.body.login === password) {
      let db = fs.readFileSync("db.json", (err, data) => {
        if (err) throw err;
        return data;
    });
    db = JSON.parse(db);
    const updatedAboutUs = req.body.toRead;
    db.aboutUs = updatedAboutUs;
    fs.writeFileSync("db.json", JSON.stringify(db));
    res.json({"res": "wszystko ok"});
    }
  });

  app.patch('/admin/honey-descriptions', (req, res) => {
    if(req.body.login === password) {
      let db = fs.readFileSync("db.json", (err, data) => {
        if (err) throw err;
        return data;
    });
    db = JSON.parse(db);
    const updatedPrices = req.body.toRead;
    db.prices = updatedPrices;
    fs.writeFileSync("db.json", JSON.stringify(db));
    res.json({"res": "wszystko ok"});
    }
  })

  app.patch('/admin/order', (req, res) => {
    mailer.function(req.body, "manual")
    let form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    form = JSON.parse(form);
    let orders = form.orders;
    let idToChange = req.body.id;
    let order = orders.filter(order=>order.id===idToChange)[0]
    order.isConfirmed = true;
    fs.writeFileSync("form.json", JSON.stringify(form));
    res.json({response: true});
  });

  app.delete('/admin/order', (req, res) => {
    let form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    form = JSON.parse(form);
    let orders = form.orders;
    let idToChange = req.body.id;
    orders = orders.filter(order => order.id !== idToChange);
    form.orders = orders;
    fs.writeFileSync("form.json", JSON.stringify(form));
    res.json({response: true});
  })

  app.delete('/admin/message', (req, res) => {
    let form = fs.readFileSync("form.json", (err, data) => {
      if (err) throw err;
      return data
    });
    form = JSON.parse(form);
    let messages = form.messages;
    let idToChange = req.body.id;
    messages = messages.filter(order => order.id !== idToChange);
    form.messages = messages;
    fs.writeFileSync("form.json", JSON.stringify(form));
    res.json({response: true});
  })

app.listen(port, '0.0.0.0', () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
